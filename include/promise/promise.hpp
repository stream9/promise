//! @file
#ifndef WHATWG_STREAM_PROMISE_HPP
#define WHATWG_STREAM_PROMISE_HPP

#include <functional>
#include <memory>
#include <stdexcept>
#include <type_traits>

namespace promise {

//! @cond PRIVATE
template<typename T> class promise;
//! @endcond

//! @brief meta function that check if given type is a promise
//! @tparam T type to check
//! @return value true if T is a promise, false otherwise
template<typename T>
struct is_promise : std::false_type {};

//! @cond PRIVATE
template<typename T>
struct is_promise<promise<T>> : std::true_type {};
//! @endcond

template<typename T>
constexpr bool is_promise_v = is_promise<T>::value;

template<typename T = void>
class promise
{
    static_assert(!is_promise_v<T>);

    class impl;

    using impl_ptr = std::shared_ptr<impl>;
public:
    using value_type = T;

public:
    //! @tparam Executor function that execute asynchronous operation.
    //!         It has to have signature of void(Resolve, Reject).
    //!         - **Resolve**: function object to call when asynchronous
    //!                         operation has successfully completed.
    //!         - **Reject**: function object to call when asynchronous
    //!                        operation has failed.
    template<typename Executor>
    promise(Executor);

    //! @brief register fulfillment handler
    //! @tparam FulfillHander function to be called when promise settled
    //!                       with fulfillment.
    //! @return pending promise which type is same as return value of
    //!         FulfillHandler.
    template<typename FulfillHandler>
    auto then(FulfillHandler);

    //! @brief register rejection handler
    //! @tparam RejectHander function to be called when promise settled
    //!                      with rejection.
    //! @return pending promise which type is same as return value of
    //!         RejectHandler.
    template<typename RejectHandler>
    auto catch_(RejectHandler);

    //! @brief register handler that is called when promise either
    //!        fulfilled or rejected
    //! @tparam FinallyHander function to be called when promise settled
    //!                       with either fulfillment or rejection.
    //! @return pending promise which type is same as this promise.
    template<typename FinallyHandler>
    promise<T> finally(FinallyHandler);

private:
    //! @cond PRIVATE
    template<typename U> friend class promise;
    //! @endcond

    // special constructor for all() function
    template<typename Executor>
    promise(Executor, size_t num_promises);

    //! @brief return promise which will be settled when all promises have
    //!        fulfilled or one of them have rejected.
    //! @tparam PromiseRange range of promises. all promises in it have to
    //!                      have same type.
    //! @return pending promise which type is same as an element of PromiseRange.
    template<typename PromiseRange>
    friend auto all(PromiseRange const&);

    //! @brief return promise which will be settled when any one of given
    //!        promises have settled either fulfillment or rejection.
    //! @tparam PromiseRange range of promises. all promises in it have to
    //!                      have same type.
    //! @return pending promise which type is same as an element of PromiseRange.
    template<typename PromiseRange>
    friend auto race(PromiseRange const&);

private:
    impl_ptr m_impl;
};

//! @brief exception class that indicates promise is rejected
class promise_rejected : public std::runtime_error
{
public:
    promise_rejected(std::string_view reason)
        : std::runtime_error { std::string(reason) }
    {}
};

/*
 * free function
 */
//! @brief return promise which resolved with given value
template<typename T>
promise<T> resolve(T const&);

//! @cond
template<typename T, size_t N>
promise<T[N]> resolve(T (&)[N]);
//! @endcond

//! @brief return promise which resolved with no value
promise<void> resolve();

//! @brief return promise which rejected with given reason
template<typename T = void>
promise<T> reject(std::string_view reason);

} // namespace promise

#include "promise.ipp"

#endif // WHATWG_STREAM_PROMISE_HPP
