#include "promise.hpp"

#include "detail/impl.hpp"

#include <cassert>

namespace promise {

/*
 * promise
 */
template<typename T>
template<typename Executor>
promise<T>::
promise(Executor init)
    : m_impl { impl::create(init) }
{}

template<typename T>
template<typename Executor>
promise<T>::
promise(Executor init, size_t const num_promises)
    : m_impl { impl::create_all_promise(init, num_promises) }
{}

template<typename T>
template<typename Handler>
auto promise<T>::
then(Handler handler)
{
    static_assert(std::is_invocable_v<Handler, T>,
        "then handler take wrong type of argument.");

    using result_t = std::invoke_result_t<Handler, T>;

    if constexpr (is_promise_v<result_t>) {
        using value_type = typename result_t::value_type;

        // result_t is a void promise
        if constexpr(std::is_same_v<value_type, void>) {
            auto init = [=](auto resolve, auto reject) {
                m_impl->on_fulfill([=](auto&& result) {
                    try {
                        auto p = handler(result);
                        p.m_impl->on_fulfill([=] { resolve(); });
                        p.m_impl->on_reject(
                            [=](auto const& e) { reject(e); }
                        );
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
            };

            return promise<void>(init);
        }
        // result_t is a promise of other type
        else {
            auto init = [=](auto resolve, auto reject) {
                m_impl->on_fulfill([=](auto&& result) {
                    try {
                        auto p = handler(result);
                        p.m_impl->on_fulfill(
                            [=](auto&& result) {
                                resolve(result);
                            });
                        p.m_impl->on_reject(
                            [=](auto const& e) { reject(e); }
                        );
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
            };

            return promise<value_type>(init);
        }
    }
    else if constexpr (std::is_same_v<result_t, void>) {
        return promise<result_t> {
            [&](auto resolve, auto reject) {
                m_impl->on_fulfill([=](auto&& result) {
                    try {
                        handler(result);
                        resolve();
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
                m_impl->on_reject([=](auto const& e) {
                    reject(e);
                });
            }
        };
    }
    else { // result_t is neither promise nor void
        return promise<result_t> {
            [&](auto resolve, auto reject) {
                m_impl->on_fulfill([=](auto&& result) {
                    try {
                        resolve(handler(result));
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
                m_impl->on_reject([=](auto const& e) {
                    reject(e);
                });
            }
        };
    }
}

template<>
template<typename Handler>
auto promise<void>::
then(Handler handler)
{
    static_assert(std::is_invocable_v<Handler>,
        "then handler must not take an argument on void promise.");

    using result_t = std::invoke_result_t<Handler>;

    if constexpr (is_promise_v<result_t>) {
        using value_type = typename result_t::value_type;

        // result_t is a void promise
        if constexpr(std::is_same_v<value_type, void>) {
            auto init = [=](auto resolve, auto reject) {
                m_impl->on_fulfill([=] {
                    try {
                        auto p = handler();
                        p.m_impl->on_fulfill([=] { resolve(); });
                        p.m_impl->on_reject(
                            [=](auto const& e) { reject(e); }
                        );
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
            };

            return promise<value_type>(init);
        }
        // result_t is a promise of other type
        else {
            auto init = [=](auto resolve, auto reject) {
                m_impl->on_fulfill([=] {
                    try {
                        auto p = handler();
                        p.m_impl->on_fulfill(
                            [=](auto&& result) { resolve(result); }
                        );
                        p.m_impl->on_reject(
                            [=](auto const& e) { reject(e); }
                        );
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
            };

            return promise<value_type>(init);
        }
    }
    else if constexpr (std::is_same_v<result_t, void>) {
        auto init = [=](auto resolve, auto reject) {
            m_impl->on_fulfill([=] {
                try {
                    handler(); resolve();
                }
                catch (...) {
                    reject(std::current_exception());
                }
            });
            m_impl->on_reject([=](auto const& e) {
                reject(e);
            });
        };

        return promise<result_t>(init);
    }
    else { // result_t is neither promise nor void
        return promise<result_t> {
            [=](auto resolve, auto reject) {
                m_impl->on_fulfill([=] {
                    try {
                        resolve(handler());
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
                m_impl->on_reject([=](auto const& e) {
                    reject(e);
                });
            }
        };
    }
}

template<typename T>
template<typename Handler>
auto promise<T>::
catch_(Handler handler)
{
    static_assert(std::is_invocable_v<Handler, std::exception_ptr>,
        "reject handler take wrong type of argument.");

    using result_t = std::invoke_result_t<Handler, std::exception_ptr>;

    static_assert(std::is_same_v<result_t, T>
               || std::is_same_v<result_t, promise<T>>,
        "reject handler has to return same type as prior promise.");

    if constexpr (is_promise_v<result_t>) {
        using value_type = typename result_t::value_type;

        // result_t is a void promise
        if constexpr(std::is_same_v<value_type, void>) {
            auto init = [=](auto resolve, auto reject) {
                m_impl->on_fulfill([=] { resolve(); });
                m_impl->on_reject([=](auto const& e) {
                    try {
                        auto p = handler(e);
                        p.m_impl->on_fulfill([=] { resolve(); });

                        p.m_impl->on_reject(
                            [=](auto const& e) { reject(e); });
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
            };

            return promise<void>(init);
        }
        // result_t is a promise of other type
        else {
            auto init = [=](auto resolve, auto reject) {
                m_impl->on_fulfill([=](auto&& result) { resolve(result); });
                m_impl->on_reject([=](auto const& e) {
                    try {
                        auto p = handler(e);
                        p.m_impl->on_fulfill(
                            [=](auto&& result) { resolve(result); });

                        p.m_impl->on_reject(
                            [=](auto const& e) { reject(e); });
                    }
                    catch (...) {
                        reject(std::current_exception());
                    }
                });
            };

            return promise<value_type>(init);
        }
    }
    else if constexpr (std::is_same_v<result_t, void>) {
        auto init = [=](auto resolve, auto reject) {
            m_impl->on_fulfill([=] { resolve(); });
            m_impl->on_reject([=](auto const& e) {
                try {
                    handler(e); resolve();
                }
                catch (...) {
                    reject(std::current_exception());
                }
            });
        };

        return promise<void>(init);
    }
    else { // result_t is neither promise nor void
        auto init = [=](auto resolve, auto reject) {
            m_impl->on_fulfill([=](auto&& result) { resolve(result); });
            m_impl->on_reject([=](auto const& e) {
                try {
                    resolve(handler(e));
                }
                catch (...) {
                    reject(std::current_exception());
                }
            });
        };

        return promise<result_t>(init);
    }
}

template<typename T>
template<typename Handler>
promise<T> promise<T>::
finally(Handler handler)
{
    return promise<T> {
        [=](auto resolve, auto reject) {
            m_impl->on_fulfill([=](auto const& result) {
                try {
                    handler();
                    resolve(result);
                }
                catch (...) {
                    reject(std::current_exception());
                }
            });
            m_impl->on_reject([=](auto const& e) {
                try {
                    handler();
                    reject(e);
                }
                catch (...) {
                    reject(std::current_exception());
                }
            });
        }
    };
}

template<>
template<typename Handler>
promise<void> promise<void>::
finally(Handler handler)
{
    return promise<void> {
        [=](auto resolve, auto reject) {
            m_impl->on_fulfill([=] {
                try {
                    handler();
                    resolve();
                }
                catch (...) {
                    reject(std::current_exception());
                }
            });
            m_impl->on_reject([=](auto const& e) {
                try {
                    handler();
                    reject(e);
                }
                catch (...) {
                    reject(std::current_exception());
                }
            });
        }
    };
}

/*
 * free function
 */

template<typename T>
promise<T>
resolve(T const& v)
{
    return promise<std::remove_cv_t<T>> {
        [&](auto resolve, auto) {
            resolve(v);
        }
    };
}

template<typename T, size_t N>
promise<T[N]>
resolve(T (&v)[N])
{
    return promise<T[N]> {
        [&](auto resolve, auto) {
            resolve(v);
        }
    };
}

inline promise<void>
resolve()
{
    return promise<void> {
        [](auto resolve, auto) {
            resolve();
        }
    };
}

template<typename T>
promise<T>
reject(std::string_view const reason)
{
    return promise<T> {
        [&](auto, auto reject) {
            reject(std::make_exception_ptr(
                    promise_rejected(reason) ));
        }
    };
}

template<typename PromiseRange>
auto
all(PromiseRange const& promises)
{
    using element_t = typename PromiseRange::value_type;
    static_assert(is_promise_v<element_t>);

    using value_t = typename element_t::value_type;

    static_assert(!std::is_array_v<value_t>,
        "Array promise isn't supported on this function"); // because it can't create a return value which is a vector of array.

    if constexpr (std::is_same_v<value_t, void>) {
        return promise<void> {
            [&](auto resolve, auto reject) {
                for (auto const& p: promises) {
                    p.m_impl->on_fulfill([=] {
                        resolve();
                    });
                    p.m_impl->on_reject([=](auto const& e) {
                        reject(e);
                    });
                }
            },
            promises.size()
        };
    }
    else if constexpr (std::is_reference_v<value_t>) {
        using result_t = std::vector<
            std::reference_wrapper<
                std::remove_reference_t<value_t> >>;

        return promise<result_t> {
            [&](auto resolve, auto reject) {
                for (auto const& p: promises) {
                    p.m_impl->on_fulfill([=](auto&& result) {
                        resolve(std::ref(result));
                    });
                    p.m_impl->on_reject([=](auto const& e) {
                        reject(e);
                    });
                }
            },
            promises.size()
        };
    }
    else {
        return promise<std::vector<value_t>> {
            [&](auto resolve, auto reject) {
                for (auto const& p: promises) {
                    p.m_impl->on_fulfill([=](auto&& result) {
                        resolve(result);
                    });
                    p.m_impl->on_reject([=](auto const& e) {
                        reject(e);
                    });
                }
            },
            promises.size()
        };
    }
}

template<typename PromiseRange>
auto
race(PromiseRange const& promises)
{
    using element_t = typename PromiseRange::value_type;
    static_assert(is_promise_v<element_t>);

    using value_t = typename element_t::value_type;

    if constexpr (std::is_same_v<value_t, void>) {
        return promise<void> {
            [&](auto resolve, auto reject) {
                for (auto const& p: promises) {
                    p.m_impl->on_fulfill([=] {
                        resolve();
                    });
                    p.m_impl->on_reject([=](auto const& e) {
                        reject(e);
                    });
                }
            }
        };
    }
    else {
        return promise<value_t> {
            [&](auto resolve, auto reject) {
                for (auto const& p: promises) {
                    p.m_impl->on_fulfill([=](auto&& result) {
                        resolve(result);
                    });
                    p.m_impl->on_reject([=](auto const& e) {
                        reject(e);
                    });
                }
            }
        };
    }
}

} // namespace promise
