#ifndef PROMISE_IMPL_HPP
#define PROMISE_IMPL_HPP

#include <exception>
#include <memory>
#include <string_view>
#include <vector>

#include "impl_base.hpp"

namespace promise {

template<typename T>
class promise<T>::impl : public std::enable_shared_from_this<impl>
                       , impl_base<T, impl>
{
    using base_t = impl_base<T, impl>;
    friend base_t;
public:
    enum class status_t { pending, fulfilled, rejected };

public:
    template<typename Executor>
    static impl_ptr
        create(Executor const init)
    {
        auto self = std::make_shared<impl>();

        try {
            base_t::run_initializer(self, init);
        }
        catch (...) {
            if (self->m_state == status_t::pending) {
                self->reject(std::current_exception());
            }
            else {
                throw;
            }
        }

        return self;
    }

    template<typename Executor>
    static impl_ptr
        create_all_promise(Executor init, size_t const num_promises)
    {
        auto self = std::make_shared<impl>();

        // only an iterator can throw exception in the called function,
        // so in case that happen, just let the exception to bubble up.
        base_t::run_all_promise_initializer(self, init, num_promises);

        return self;
    }

    template<typename Handler>
    void on_fulfill(Handler handler)
    {
        switch (m_state) {
            case status_t::pending:
                m_fulfill_handlers.push_back(handler);
                break;
            case status_t::fulfilled:
                this->launch_fulfill_handler(handler);
                break;
            case status_t::rejected:
            default:
                break;
        }
    }

    template<typename Handler>
    void on_reject(Handler handler)
    {
        switch (m_state) {
            case status_t::pending:
                m_reject_handlers.push_back(handler);
                break;
            case status_t::rejected:
                this->launch_reject_handler(handler);
                break;
            case status_t::fulfilled:
            default:
                break;
        }
    }

    void reject(std::string_view const reason)
    {
        if (m_state != status_t::pending) return;

        reject(std::make_exception_ptr(
            promise_rejected(reason))
        );
    }

    void reject(std::exception_ptr const ptr)
    {
        if (m_state != status_t::pending) return;

        m_state = status_t::rejected;
        m_error = ptr;

        m_has_unhandled_exception = true;

        launch_reject_handlers();
    }

    template<typename Handler>
    void launch_reject_handler(Handler handler)
    {
        handler(m_error);
        m_has_unhandled_exception = false;
    }

    void launch_fulfill_handlers()
    {
        for (auto const& handler: m_fulfill_handlers) {
            this->launch_fulfill_handler(handler);
        }

        m_fulfill_handlers.clear();
        m_reject_handlers.clear();
    }

    void launch_reject_handlers()
    {
        for (auto const& handler: m_reject_handlers) {
            this->launch_reject_handler(handler);
        }

        m_fulfill_handlers.clear();
        m_reject_handlers.clear();
    }

private:
    using fulfill_fn = typename base_t::fulfill_fn;
    using reject_fn = typename base_t::reject_fn;

    status_t m_state = status_t::pending;
    std::exception_ptr m_error;

    std::vector<fulfill_fn> m_fulfill_handlers;
    std::vector<reject_fn> m_reject_handlers;

    bool m_has_unhandled_exception = false;
};

} // namespace promise

#endif // PROMISE_IMPL_HPP
