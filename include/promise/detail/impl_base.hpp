#ifndef PROMISE_IMPL_BASE_HPP
#define PROMISE_IMPL_BASE_HPP

#include <exception>
#include <functional>
#include <type_traits>

namespace promise {

/*
 * impl_base
 */
template<typename T, typename Derived>
class impl_base
{
public: // type
    using fulfill_fn = std::function<void(T const&)>;
    using reject_fn = std::function<void(std::exception_ptr const&)>;

public: // function
    template<typename Self, typename Executor>
    static void
    run_initializer(Self const self, Executor init)
    {
        init(
            [=](auto const& result) { self->fulfill(result); },
            [=](auto const& e) { self->reject(e); }
        );
    }

    template<typename Self, typename Executor>
    static void
    run_all_promise_initializer(Self const self, Executor init,
                                size_t const num_promises)
    {
        self->m_value.reserve(num_promises);

        auto resolve = [=](auto&& result) {
            self->m_value.push_back(result);

            if (self->m_value.size() == num_promises) {
                self->fulfill(self->m_value);
            }
        };

        auto reject = [=](auto const& e) {
            self->reject(e);
        };

        init(resolve, reject);
    }

    void fulfill(T result)
    {
        if (derived().m_state != Derived::status_t::pending) return;

        derived().m_state = Derived::status_t::fulfilled;
        m_value = std::move(result);

        derived().launch_fulfill_handlers();
    }

    template<typename Fulfilled>
    void launch_fulfill_handler(Fulfilled handler)
    {
        handler(m_value);
    }

    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

private: // variable
    T m_value;
};

/* specialization for void */
template<typename Derived>
class impl_base<void, Derived>
{
public: // type
    using fulfill_fn = std::function<void()>;
    using reject_fn = std::function<void(std::exception_ptr const&)>;

public: // function
    template<typename Self, typename Executor>
    static void
    run_initializer(Self const self, Executor init)
    {
        init(
            [=]() { self->fulfill(); },
            [=](auto const& e) { self->reject(e); }
        );
    }

    template<typename Self, typename Executor>
    static void
    run_all_promise_initializer(Self const self, Executor init,
                                size_t const num_promises)
    {
        self->m_num_pending = num_promises;

        auto resolve = [=] {
            --self->m_num_pending;
            if (self->m_num_pending == 0) {
                self->fulfill();
            }
        };

        auto reject = [=](auto const& e) {
            self->reject(e);
        };

        init(resolve, reject);
    }

    void fulfill()
    {
        if (derived().m_state != Derived::status_t::pending) return;

        derived().m_state = Derived::status_t::fulfilled;

        derived().launch_fulfill_handlers();
    }

    template<typename Fulfilled>
    void launch_fulfill_handler(Fulfilled handler)
    {
        handler();
    }

    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

private:
    size_t m_num_pending = 0; //TODO This variable is used only for all promises
};

/* specialization for pointer */
template<typename T, typename Derived>
class impl_base<T*, Derived>
{
public: // type
    using fulfill_fn = std::function<void(T* const)>;
    using reject_fn = std::function<void(std::exception_ptr const&)>;

public: // function
    template<typename Self, typename Executor>
    static void
    run_initializer(Self const self, Executor init)
    {
        init(
            [=](auto* const result) { self->fulfill(result); },
            [=](auto const& e) { self->reject(e); }
        );
    }

    void fulfill(T* const result)
    {
        if (derived().m_state != Derived::status_t::pending) return;

        derived().m_state = Derived::status_t::fulfilled;
        m_value = result;

        derived().launch_fulfill_handlers();
    }

    template<typename Fulfilled>
    void launch_fulfill_handler(Fulfilled handler)
    {
        handler(m_value);
    }

    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

private: // variable
    T* m_value;
};

/* specialization for array */
template<typename T, typename Derived, size_t N>
class impl_base<T[N], Derived>
{
public: // type
    using fulfill_fn = std::function<void(T[N])>;
    using reject_fn = std::function<void(std::exception_ptr const&)>;

public: // function
    template<typename Self, typename Executor>
    static void
    run_initializer(Self const self, Executor init)
    {
        init(
            [=](T (&result)[N]) { self->fulfill(result); },
            [=](auto const& e) { self->reject(e); }
        );
    }

    void fulfill(T (&result)[N])
    {
        if (derived().m_state != Derived::status_t::pending) return;

        derived().m_state = Derived::status_t::fulfilled;
        m_value = result;

        derived().launch_fulfill_handlers();
    }

    template<typename Fulfilled>
    void launch_fulfill_handler(Fulfilled handler)
    {
        handler(m_value);
    }

    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

private: // variable
    T* m_value;
};

/* specialization for reference */
template<typename T, typename Derived>
class impl_base<T&, Derived>
{
public: // type
    using fulfill_fn = std::function<void(T)>;
    using reject_fn = std::function<void(std::exception_ptr const&)>;

public: // function
    template<typename Self, typename Executor>
    static void
    run_initializer(Self const self, Executor init)
    {
        init(
            [=](T& result) { self->fulfill(&result); },
            [=](auto const& e) { self->reject(e); }
        );
    }

    void fulfill(T* const result)
    {
        if (derived().m_state != Derived::status_t::pending) return;

        derived().m_state = Derived::status_t::fulfilled;
        m_value = result;

        derived().launch_fulfill_handlers();
    }

    template<typename Fulfilled>
    void launch_fulfill_handler(Fulfilled handler)
    {
        handler(*m_value);
    }

    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

private: // variable
    T* m_value;
};

} // namespace promise

#endif // PROMISE_IMPL_BASE_HPP
