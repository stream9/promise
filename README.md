# promise
C++ implementaion of promise data structure based on design of ECMAScript's Promise.

## Overview
It is designed based on ECMAScript's Promise but it is statically typed 
according to the result's value.
As a consequence of static typing, there are several differences against ECMAScript.

1. then() member function only take fulfillment handler  
   ECMAScript's Promise allow to take two handler as then()'s argument. 
   One is fulfillment and the 
   other is reject handler. The problem is then() returns another promise
   which resolve with handlers result, but both handlers can return different
   type of result, which lead to the ambiguity of the type of promise then()
   returns.

2. catch() function is named as "catch_()"  
   Because "catch" is a language's reserve word. I could name it with other similar
   name (like error()) but I took the similarity to the original design. 
   By doing this way, I have to remember one less thing.

3. rejection handler's argument is fixed to std::exception_ptr  
   Asynchronous operations can reject promise by not only calling reject() with 
   reason string, but also throwing exceptions. To handle both situation with
   same entry point, rejection handler take std::exception_ptr as its argument.
   reject() call will become std::exception_ptr to a instance of 'promise_rejected' class.

4. rejection handler have to return same type of value as prior promise  
   In order to propagate resolution of prior promise to promises down the
   line of the chain, catch_() have to return same type of promise as prior one.
   To do that, rejection handler have to return same type of value as prior promise.

# Requrement
- C++17 compliant compiler
- boost (only for testing)

# Getting Started
It is designed to be used as cmake sub project. But you can just copy include
directory and set relevant compiler option by yourself if you want.

# [API Reference](https://stream9.gitlab.io/promise/promise_8hpp.html)

# Example
```c++
#include <promise/promise.hpp>

promise::promise<int> async_job()
{
    return promise::promise<int> {
        [](auto resolve, auto reject) {
            auto callback = [=](auto const& err, auto const& result) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            };
            some_asynchronous_function(callback);
        }
    };
}
...

// attach handler to promise
async_job()
    .then([](auto const& result) {
        std::cout << "resolved: " << result << std::endl;
    })
    .catch_([](auto const& e) {
        try {
            std::rethrow_exception(e);
        }
        catch(auto const& err) {
            std::cout << "failed: err = " << err << std::endl;
        }
        return 0; // look design section for the reason of this return value
    })
    .finally([] {
        do_some_clean_up();
    });

// compound promise that resolves when all of its constituents have resolved.
std::vector promises1 {
    async_job(),
    async_job(),
    async_jon(),
};

promise::all(promises1)
    .then([](std::vector<int> const& values) {
        assert(values.size() == 3);
    });

// compound promise that resolves with value of first resolved / rejected promise.
std::vector promises2 {
    async_job(),
    async_job(),
};

promise::race(promises2)
    .then([](int const value) {
        std::cout << "first promise result in " << value << std::endl;
    });
```
