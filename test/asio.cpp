#include <promise/promise.hpp>

#include <chrono>
#include <iostream>
#include <type_traits>

#include <boost/test/unit_test.hpp>
#include <boost/asio.hpp>

namespace promise::testing {

namespace asio = boost::asio;
using namespace std::literals;

class asio_fixture
{
public:
    template<typename T>
    auto
    resolve_timer(std::chrono::milliseconds const d,
                  T value)
    {
        return promise<int> {
            [&](auto resolve, auto) {
                set_timeout(
                    [=](auto&&) { resolve(value); },
                    d
                );
            }
        };
    }

    auto
    reject_timer(std::chrono::milliseconds const d)
    {
        return promise<int> {
            [&](auto, auto reject) {
                set_timeout(
                    [=](auto&&) { reject(""); },
                    d
                );
            }
        };
    }

    void run()
    {
        m_io.run();
    }

    template<typename Callback>
    void
    set_timeout(Callback callback,
                std::chrono::milliseconds const duration)
    {
        m_timers.emplace_back(m_io, duration);
        m_timers.back().async_wait(callback);
    }

private:
    asio::io_context m_io;
    std::vector<asio::steady_timer> m_timers;
};

BOOST_FIXTURE_TEST_SUITE(asio_, asio_fixture)

    BOOST_AUTO_TEST_CASE(timer_resolve)
    {
        bool resolved = false;
        bool rejected = false;

        resolve_timer(100ms, 1)
            .then([&](auto const& i) {
                BOOST_CHECK_EQUAL(i, 1);
                resolved = true;
            });

        run();

        BOOST_CHECK(resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(timer_reject)
    {
        bool resolved = false;
        bool rejected = false;

        reject_timer(100ms)
            .then([&](auto&&) { resolved = true; })
            .catch_([&](auto const& e) {
                BOOST_CHECK_THROW((
                    std::rethrow_exception(e)), promise_rejected);

                rejected = true;
            });

        run();

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

    BOOST_AUTO_TEST_CASE(timer_fulfill_handler_throw)
    {
        resolve_timer(100ms, 1)
            .then([](auto const& i) { // -> promise<void>
                BOOST_CHECK_EQUAL(i, 1);
                throw 1;
            })
            .catch_([](auto const& e) {
                BOOST_CHECK_THROW(std::rethrow_exception(e), int);
            });

        resolve_timer(100ms, 1)
            .then([](auto const i) { // -> promise<int>
                throw i;
                return ::promise::resolve(1);
            })
            .catch_([](auto const& e) {
                BOOST_CHECK_THROW(std::rethrow_exception(e), int);
                return 3;
            });

        // throw from initializer of promise<int> which is return
        // from fulfillhander
        resolve_timer(100ms, 1)
            .then([](auto const i) {
                return promise<int> {
                    [=](auto, auto) {
                        throw i;
                    }
                };
            })
            .catch_([](auto const& e) {
                BOOST_CHECK_THROW(std::rethrow_exception(e), int);
                return 4;
            });

        // throw from fulfill handler which is attached to promise<int>
        // that is return from fulfill handler
        resolve_timer(100ms, 1)
            .then([](auto) {
                auto p = ::promise::resolve(1);
                p.then([](auto) { throw 1; });
                return p;
            })
            .catch_([](auto const& e) {
                BOOST_CHECK_THROW(std::rethrow_exception(e), int);
                return 5;
            });

        run();
    }

    BOOST_AUTO_TEST_CASE(timer_all_resolved)
    {
        std::vector promises {
            resolve_timer(100ms, 1),
            resolve_timer(200ms, 2),
            resolve_timer(300ms, 3),
        };

        bool resolved = false;
        bool rejected = false;

        all(promises)
            .then(
                [&](auto const& values) {
                    resolved = true;
                    BOOST_CHECK_EQUAL(values.size(), 3);
                }
            )
            .catch_([&](auto&&) {
                rejected = true;
            });

        run();

        BOOST_CHECK(resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(timer_all_rejected_last)
    {
        std::vector promises {
            resolve_timer(100ms, 1),
            resolve_timer(200ms, 2),
            reject_timer(300ms), // reject last
        };

        bool resolved = false;
        bool rejected = false;

        all(promises)
            .then([&](auto&&) { resolved = true; })
            .catch_([&](auto&&) { rejected = true; });

        run();

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

    BOOST_AUTO_TEST_CASE(timer_all_rejected_first)
    {
        std::vector promises {
            reject_timer(100ms),  // reject first
            resolve_timer(200ms, 2),
            resolve_timer(300ms, 3),
        };

        bool resolved = false;
        bool rejected = false;

        all(promises)
            .then([&](auto&&) { resolved = true; })
            .catch_([&](auto&&) { rejected = true; });

        run();

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

    BOOST_AUTO_TEST_CASE(timer_race_resolved)
    {
        std::vector promises {
            reject_timer(300ms),
            resolve_timer(200ms, 2),
            resolve_timer(100ms, 1),
        };

        bool resolved = false;
        bool rejected = false;

        ::promise::race(promises)
            .then([&](auto const& v) {
                BOOST_CHECK_EQUAL(v, 1);
                resolved = true;
            })
            .catch_([&](auto&&) { rejected = true; });

        run();

        BOOST_CHECK(resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(timer_race_rejected)
    {
        std::vector promises {
            resolve_timer(300ms, 3),
            resolve_timer(200ms, 2),
            reject_timer(100ms),
        };

        bool resolved = false;
        bool rejected = false;

        ::promise::race(promises)
            .then([&](auto&&) { resolved = true; })
            .catch_([&](auto&&) { rejected = true; });

        run();

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

BOOST_AUTO_TEST_SUITE_END() // asio_

} // namespace promise::testing


