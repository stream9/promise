#include <promise/promise.hpp>

#include <iostream>
#include <type_traits>

#include <boost/test/unit_test.hpp>

namespace promise::testing {

template<class> class type_of;

BOOST_AUTO_TEST_SUITE(promise_)

    BOOST_AUTO_TEST_CASE(void_promise_then_)
    {
        promise<void> p1 {
            [](auto/*resolve*/, auto/*reject*/) {}
        };

        // handler return void
        p1.then([] {});

        // handler return int
        p1.then([] { return 1; })
          .then([](auto i) { BOOST_CHECK_EQUAL(i, 1); });

        // handler return promise<void>
        p1.then([] {
            return promise<void> {
                [](auto, auto) {}
            };
        });

        // handler returns promise<int>
        p1.then([] {
            return promise<int> {
                [](auto resolve, auto) { resolve(1); }
            };
        })
        .then([](auto i) { BOOST_CHECK_EQUAL(i, 1); });
    }

    BOOST_AUTO_TEST_CASE(non_void_promise_then)
    {
        promise<int> p1 {
            [](auto resolve, auto/*reject*/) { resolve(1); }
        };

        // handler return void
        p1.then([](auto i) { BOOST_CHECK_EQUAL(i, 1); });

        // handler return int
        p1.then([](auto i) { BOOST_CHECK_EQUAL(i, 1); return i + 1; })
          .then([](auto i) { BOOST_CHECK_EQUAL(i, 2); });

        // handler return promise<void>
        p1.then([](auto i) {
            BOOST_CHECK_EQUAL(i, 1);

            return promise<void> {
                [](auto, auto) {}
            };
        });

        // handler returns promise<int>
        p1.then([](auto i) {
            return promise<int> {
                [=](auto resolve, auto) { resolve(i); }
            };
        })
        .then([](auto i) { BOOST_CHECK_EQUAL(i, 1); });
    }

    BOOST_AUTO_TEST_CASE(catch_)
    {
        promise<void> p1 {
            [](auto/*resolve*/, auto reject) {
                reject("rejected");
            }
        };

        // handler return nothing
        p1.catch_([](auto const& e) {
            BOOST_CHECK_THROW(
                std::rethrow_exception(e),
                promise_rejected
            );
        });

        // handler return resolved void promise
        p1.catch_([](auto const&) {
            return promise<void>(
                [](auto resolve, auto) {
                    resolve();
                });
        })
        .then([]{});

        // handler return rejected void promise
        p1.catch_([](auto const&) {
            return promise<void>(
                [](auto, auto reject) {
                    reject("rejected 2");
                });
        })
        .catch_([](auto const& e) {
            try {
                std::rethrow_exception(e);
            }
            catch (promise_rejected const& ex) {
                BOOST_CHECK_EQUAL(ex.what(), "rejected 2");
            }
        });
    }

    template<typename> class tpye_of;

    BOOST_AUTO_TEST_CASE(static_builders)
    {
        auto p1 = ::promise::resolve();
        static_assert(std::is_same_v<decltype(p1)::value_type, void>);
        p1.then([] {});

        auto p2 = ::promise::resolve(1);
        static_assert(std::is_same_v<decltype(p2)::value_type, int>);
        p2.then([](auto const i) {
            BOOST_CHECK_EQUAL(i, 1);
        });

        int i = 3;
        const int j = 4;

        auto p3 = ::promise::resolve(i);
        static_assert(std::is_same_v<decltype(p3), promise<int>>);
        p3.then([](int const i) {
            BOOST_CHECK_EQUAL(i, 3);
        });

        auto p4 = ::promise::resolve(j);
        static_assert(std::is_same_v<decltype(p4), promise<int>>);
        p4.then([](int const& i) {
            BOOST_CHECK_EQUAL(i, 4);
        });

        auto p5 = ::promise::resolve<int&>(i);
        static_assert(std::is_same_v<decltype(p5), promise<int&>>);
        p5.then([](int const& i) {
            BOOST_CHECK_EQUAL(i, 3);
        });

        auto p6 = ::promise::resolve<int const&>(i);
        static_assert(std::is_same_v<decltype(p6), promise<int const&>>);
        p6.then([](int const& i) {
            BOOST_CHECK_EQUAL(i, 3);
        });
    }

    BOOST_AUTO_TEST_CASE(reject_)
    {
        auto p1 = ::promise::reject("");
        p1.catch_([](auto const& e) {
            BOOST_CHECK_THROW(
                std::rethrow_exception(e),
                promise_rejected
            );
        });
    }

    BOOST_AUTO_TEST_CASE(intializer_throws_exception)
    {
        promise<void> p1 {
            [](auto, auto) {
                throw 1;
            }
        };

        p1.catch_([](auto const& e) {
            BOOST_CHECK_THROW(
                std::rethrow_exception(e),
                int
            );
        });

        promise<int> p2 {
            [](auto, auto) {
                throw 1;
            }
        };

        p2.catch_([](auto const& e) {
            BOOST_CHECK_THROW(
                std::rethrow_exception(e),
                int
            );
            return 0;
        });

        // exception thrown after promise resolved
        auto make_promise = [] {
            return promise<int> {
                [](auto resolve, auto) {
                    resolve(1);
                    throw 1;
                }
            };
        };

        BOOST_CHECK_THROW(make_promise(), int);
    }

    BOOST_AUTO_TEST_CASE(handler_throws_exception)
    {
        auto p1 = ::promise::resolve();

        p1.then([] {
            throw 1;
        })
        .catch_([](auto const& e) {
            BOOST_CHECK_THROW(
                std::rethrow_exception(e),
                int
            );
        });

        auto p2 = ::promise::resolve(1);

        p2.then([](auto){
            throw 1;
        })
        .catch_([](auto const& e) {
            BOOST_CHECK_THROW(
                std::rethrow_exception(e),
                int
            );
        });
    }

    BOOST_AUTO_TEST_CASE(uncaught_exception)
    {
#if 0
        promise<void> p1 {
            [](auto, auto) {
                throw 1;
            }
        };
#endif
    }

    BOOST_AUTO_TEST_CASE(reference_promise)
    {
        int i = 0;

        promise<int&> p1 {
            [&](auto resolve, auto) { resolve(++i); }
        };

        p1.then([](int& j) -> int& {
            BOOST_CHECK_EQUAL(j, 1);
            return ++j;
        })
        .then([&](int& k) {
            BOOST_CHECK_EQUAL(k, 2);
            BOOST_CHECK_EQUAL(i, k);
        });

        // reference promise from reference promise
        p1.then([&](int&) {
            return promise<int&> {
                [&](auto resolve, auto) { resolve(++i); }
            };
        })
        .then([](int& j) {
            BOOST_CHECK_EQUAL(j, 3);
        });

        p1.then([&](int&) {
            return promise<void> {
                [&](auto resolve, auto) { resolve(); }
            };
        })
        .then([] {});

        // reference promise from void promise
        ::promise::resolve()
            .then([&] {
                return promise<int&> {
                    [&](auto resolve, auto) { resolve(++i); }
                };
            })
            .then([](int& j) {
                BOOST_CHECK_EQUAL(j, 4);
            });

        // reference promise from catch handler
        promise<int&> p2 {
            [](auto, auto reject) {
                reject("foo");
            }
        };

        p2.catch_([&](auto&&) {
            return promise<int&> {
                [&](auto resolve, auto) { resolve(++i); }
            };
        })
        .then([](int& j) {
            BOOST_CHECK_EQUAL(j, 5);
        });
    }

    BOOST_AUTO_TEST_CASE(const_reference_promise)
    {
        int i = 5;

        promise<int const&> p1 {
            [&](auto resolve, auto) { resolve(i); }
        };

        p1.then([](int const& j) -> int const& {
            BOOST_CHECK_EQUAL(j, 5);
            return j;
        })
        .then([&](int const& k) {
            BOOST_CHECK_EQUAL(i, k);
        });
    }

    BOOST_AUTO_TEST_CASE(pointer_promise)
    {
        int i1 = 5;
        ::promise::resolve(&i1)
            .then([&](auto const& v) {
                BOOST_CHECK_EQUAL(v, &i1);
            });

        int const i2 = 10;
        ::promise::resolve(&i2)
            .then([&](auto const& v) {
                BOOST_CHECK_EQUAL(v, &i2);
            });
    }

    BOOST_AUTO_TEST_CASE(array_promise)
    {
        char arr1[] { '0', '1', '2' };

        ::promise::resolve(arr1)
            .then([&](auto const& v) {
                BOOST_CHECK_EQUAL(v, arr1);
            });

        char const arr2[] { '0', '1', '2' };

        ::promise::resolve(arr2)
            .then([&](auto const& v) {
                BOOST_CHECK_EQUAL(v, arr2);
            });
    }

    BOOST_AUTO_TEST_CASE(bool_promise)
    {
        bool v1 = true;

        ::promise::resolve(v1)
            .then([&](auto const& v) {
                BOOST_CHECK_EQUAL(v, v1);
            });

        bool const v2 = false;

        ::promise::resolve(v2)
            .then([&](auto const& v) {
                BOOST_CHECK_EQUAL(v, v2);
            });
    }

    BOOST_AUTO_TEST_CASE(string_literal_)
    {
        auto p1 = ::promise::resolve("literal");

        p1.then([](auto v[]) {
            BOOST_CHECK_EQUAL(v, "literal");
        });
    }

    BOOST_AUTO_TEST_CASE(finally_resolve_1)
    {
        auto n_then = 0;
        auto n_catch = 0;
        auto n_finally = 0;

        ::promise::resolve()
            .then([&] { ++n_then; })
            .catch_([&](auto&&) { ++n_catch; })
            .finally([&] { ++n_finally; })
            ;

        BOOST_CHECK_EQUAL(n_then, 1);
        BOOST_CHECK_EQUAL(n_catch, 0);
        BOOST_CHECK_EQUAL(n_finally, 1);
    }

    BOOST_AUTO_TEST_CASE(finally_resolve_2)
    {
        auto n_then = 0;
        auto n_catch = 0;
        auto n_finally = 0;

        ::promise::resolve(1)
            .then([&](auto&&) { ++n_then; })
            .catch_([&](auto&&) { ++n_catch; })
            .finally([&] { ++n_finally; })
            ;

        BOOST_CHECK_EQUAL(n_then, 1);
        BOOST_CHECK_EQUAL(n_catch, 0);
        BOOST_CHECK_EQUAL(n_finally, 1);
    }

    BOOST_AUTO_TEST_CASE(finally_reject_1)
    {
        auto n_then = 0;
        auto n_catch = 0;
        auto n_finally = 0;

        ::promise::reject("")
            .then([&] { ++n_then; })
            .catch_([&](auto&&) { ++n_catch; })
            .finally([&] { ++n_finally; })
            ;

        BOOST_CHECK_EQUAL(n_then, 0);
        BOOST_CHECK_EQUAL(n_catch, 1);
        BOOST_CHECK_EQUAL(n_finally, 1);
    }

    BOOST_AUTO_TEST_CASE(finally_reject_2)
    {
        auto n_then = 0;
        auto n_catch = 0;
        auto n_finally = 0;

        auto p1 = promise<int> {
            [](auto, auto reject) { reject(""); }
        };

        p1.then([&](auto&&) { ++n_then; })
          .catch_([&](auto&&) { ++n_catch; })
          .finally([&] { ++n_finally; })
          ;

        BOOST_CHECK_EQUAL(n_then, 0);
        BOOST_CHECK_EQUAL(n_catch, 1);
        BOOST_CHECK_EQUAL(n_finally, 1);
    }

    BOOST_AUTO_TEST_CASE(finally_chain_1)
    {
        auto n_then = 0;
        auto n_catch = 0;
        auto n_finally = 0;

        ::promise::resolve(2)
            .finally([&] { ++n_finally;  })
            .then([&](auto const i) { n_then += i; })
            .catch_([&](auto&&) { ++n_catch; })
            ;

        BOOST_CHECK_EQUAL(n_then, 2);
        BOOST_CHECK_EQUAL(n_catch, 0);
        BOOST_CHECK_EQUAL(n_finally, 1);
    }

    BOOST_AUTO_TEST_CASE(finally_chain_2)
    {
        auto n_then = 0;
        auto n_catch = 0;
        auto n_finally = 0;

        ::promise::reject("foo")
            .finally([&] { ++n_finally;  })
            .then([&] { ++n_then; })
            .catch_([&](auto&&) { ++n_catch; })
            ;

        BOOST_CHECK_EQUAL(n_then, 0);
        BOOST_CHECK_EQUAL(n_catch, 1);
        BOOST_CHECK_EQUAL(n_finally, 1);
    }

    BOOST_AUTO_TEST_CASE(all_int_resolved)
    {
        std::vector pv {
            ::promise::resolve(1),
            ::promise::resolve(3),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then(
            [&](auto values) {
                BOOST_REQUIRE_EQUAL(values.size(), 2);

                std::sort(values.begin(), values.end());
                BOOST_CHECK_EQUAL(values[0], 1);
                BOOST_CHECK_EQUAL(values[1], 3);

                resolved = true;
            }
        )
        .catch_([&](auto const&) { rejected = true; });

        BOOST_CHECK(resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(all_int_rejected)
    {
        std::vector pv {
            ::promise::resolve(1),
            ::promise::reject<int>("foo"),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&](auto&&) {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

    BOOST_AUTO_TEST_CASE(all_int_pending)
    {
        std::vector pv {
            promise<int> { [](auto, auto) {} },
            promise<int> { [](auto, auto) {} },
            promise<int> { [](auto, auto) {} },
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&](auto&&) {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(all_void_resolved)
    {
        std::vector pv {
            ::promise::resolve(),
            ::promise::resolve(),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&] { resolved = true; })
               .catch_([&](auto const&) { rejected = true; });

        BOOST_CHECK(resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(all_void_rejected)
    {
        std::vector pv {
            ::promise::resolve(),
            ::promise::reject("foo"),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&] {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

    BOOST_AUTO_TEST_CASE(all_void_pending)
    {
        std::vector pv {
            promise<void> { [](auto, auto) {} },
            promise<void> { [](auto, auto) {} },
            promise<void> { [](auto, auto) {} },
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&] {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(all_pointer_resolved)
    {
        int i = 1;
        int j = 3;

        std::vector pv {
            ::promise::resolve<int*>(&i),
            ::promise::resolve<int*>(&j),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then(
            [&](auto values) {
                BOOST_REQUIRE_EQUAL(values.size(), 2);

                std::sort(values.begin(), values.end());
                BOOST_CHECK_EQUAL(*values[0], 1);
                BOOST_CHECK_EQUAL(*values[1], 3);

                resolved = true;
            }
        )
        .catch_([&](auto const&) { rejected = true; });

        BOOST_CHECK(resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(all_pointer_rejected)
    {
        int i = 1;

        std::vector pv {
            ::promise::resolve<int*>(&i),
            ::promise::reject<int*>("foo"),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&](auto&&) {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

    BOOST_AUTO_TEST_CASE(all_pointer_pending)
    {
        std::vector pv {
            promise<int*> { [](auto, auto) {} },
            promise<int*> { [](auto, auto) {} },
            promise<int*> { [](auto, auto) {} },
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&](auto&&) {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(all_reference_resolved)
    {
        int i = 1;
        int j = 3;

        std::vector pv {
            ::promise::resolve<int&>(i),
            ::promise::resolve<int&>(j),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then(
            [&](auto values) {
                BOOST_REQUIRE_EQUAL(values.size(), 2);

                std::sort(values.begin(), values.end());
                BOOST_CHECK_EQUAL(values[0], 1);
                BOOST_CHECK_EQUAL(values[1], 3);

                resolved = true;
            }
        )
        .catch_([&](auto const&) { rejected = true; });

        BOOST_CHECK(resolved);
        BOOST_CHECK(!rejected);
    }

    BOOST_AUTO_TEST_CASE(all_reference_rejected)
    {
        int i = 1;

        std::vector pv {
            ::promise::resolve<int&>(i),
            ::promise::reject<int&>("foo"),
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&](auto&&) {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(rejected);
    }

    BOOST_AUTO_TEST_CASE(all_reference_pending)
    {
        std::vector pv {
            promise<int&> { [](auto, auto) {} },
            promise<int&> { [](auto, auto) {} },
            promise<int&> { [](auto, auto) {} },
        };

        bool resolved = false;
        bool rejected = false;

        all(pv).then([&](auto&&) {
            resolved = true;
        })
        .catch_([&](auto const&) {
            rejected = true;
        });

        BOOST_CHECK(!resolved);
        BOOST_CHECK(!rejected);
    }

BOOST_AUTO_TEST_SUITE_END() // promise_

} // namespace promise::testing

BOOST_AUTO_TEST_CASE(call_builder_from_global_namespace)
{
    auto p1 = promise::resolve(1);
}
